#pragma once

struct Monastery
{
    char name[ 15 ];
    char school;
    unsigned numberOfMonks;
    double square;
};

void printTable( Monastery* monasteries, int size, int countOfElements );

void addLine( Monastery* monasteries, int size, int& countOfElements );

void printLine( Monastery* monasteries, int size, int index );

void deleteLine( Monastery* monasteries, int& count, int index );

void deleteAll( Monastery* monasteries, int& count );
