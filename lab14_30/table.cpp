#include "table.h"

#include <iostream>
#include <iomanip>
#include <cstring>

void printDelimeter()
{
    std::cout << "|----------------------------------------------|" << std::endl;
}

void printHead()
{
    printDelimeter();
    std::cout << "|Monasteries of Japan                          |" << std::endl;
    std::cout << "|----------------------------------------------|" << std::endl;
    std::cout << "| Name      | Shcool | Monks     | Square      |" << std::endl;
    std::cout << "|           |        | count     |             |" << std::endl;
}

bool checkIndex( int size, int index )
{
    bool isLarger = index >= size;
    bool isLower = index < 0;
    if( isLower || isLarger )
    {
        std::cout << "Bad index!" << std::endl;
        return false;
    }
    return true;
}

void printTip()
{
    std::cout << "|Tip:---T-Thenday--S-Singon--D-Dzedzitsu-------|" << std::endl;
}

void printTable( Monastery* monasteries, int size, int countOfElements )
{
    printHead();
    if ( checkIndex( size, countOfElements ) )
    {
        for ( int i = 0; i < countOfElements; ++i )
        {
            printDelimeter();
            printLine( monasteries, size, i );
        }
    }
    printDelimeter();
    printTip();
    printDelimeter();
    return;
}

void addLine( Monastery* monasteries, int size, int& countOfElements )
{
    if ( !checkIndex( size, countOfElements ) )
        return;
    std::cout << "Enter the name, school, count of monks and square:" << std::endl;
    std::cin >>
            monasteries[countOfElements].name >>
            monasteries[countOfElements].school >>
            monasteries[countOfElements].numberOfMonks >>
            monasteries[countOfElements].square;
    ++countOfElements;
}

void printLine( Monastery* monasteries, int size, int index )
{
    if ( !checkIndex( size, index ) )
        return;
    std::cout << "| "<< std::setw( 9 ) << monasteries[index].name <<
                 " |    " << monasteries[index].school << "   |       " <<
                 std::setw( 3 ) << monasteries[index].numberOfMonks << " | " <<
                 std::setw( 5 ) << std::setprecision( 1 ) << std::fixed << std::showpoint <<
                 monasteries[index].square << "       |";
    std::cout << std::endl;
}

void deleteLine( Monastery* monasteries, int& count, int index )
{
    if ( !checkIndex( count, index ) )
        return;
    Monastery emptyMon = { { '\0' }, '\0', 0, 0.0 };
    memcpy( &monasteries[ index ], &emptyMon, sizeof( Monastery ) );
    for ( int i = index; i < count - 1; ++i )
    {
        memcpy( &monasteries[ i ], &monasteries[ i + 1 ], sizeof( Monastery ) );
    }
    --count;
    count = ( count < 0 ) ? 0 : count;
}

void deleteAll( Monastery* monasteries, int& count )
{
    Monastery emptyMon = { { '\0' }, '\0', 0, 0.0 };
    for ( int i = 0; i < count; ++i )
    {
        memcpy( &monasteries[ i ], &emptyMon, sizeof( Monastery ) );
    }
    count = 0;
}


