#include <iostream>

#include "table.h"

enum EAction
{
    ExitCode = '0',
    AddItem = '1',
    DeleteItem = '2',
    DeleteAll = '3',
    ShowItem = '4',
    ShowAll = '5'
};

int main()
{
    const int SIZE = 3;
    Monastery monasteries[ SIZE ];
    int count = 0;
    while ( true )
    {
        std::cout << "Help:" << std::endl <<
                     "0 - exit" << std::endl <<
                     "1 - add item" << std::endl <<
                     "2 - delete item" << std::endl <<
                     "3 - delete all" << std::endl <<
                     "4 - show line" << std::endl <<
                     "5 - show all" << std::endl <<
                     "Your input:" << std::endl;
        char command{ 0 };
        std::cin >> command;
        std::cout << std::endl;
        switch ( command )
        {
        case ExitCode:
            std::cout << "Good bye!" << std::endl;
            return 0;
        case AddItem:
        {
            addLine( monasteries, SIZE, count );
            break;
        }
        case DeleteItem:
        {
            if ( count > SIZE )
                count = SIZE;
            else if ( !count )
                break;
            std::cout << "Enter the index of line ( 0 - " << count - 1 << " ):" << std::endl;
            int index{ -1 };
            std::cin >> index;
            deleteLine( monasteries, count, index );
            break;
        }
        case DeleteAll:
            if ( count > SIZE )
                count = SIZE;
            deleteAll( monasteries, count );
            break;
        case ShowItem:
        {
            if ( count > SIZE )
                count = SIZE;
            else if ( !count )
                break;
            int maxIndex = ( !count ) ? 0 : count - 1;
            std::cout << "Enter the index of line ( 0 - " << maxIndex << " ):" << std::endl;
            int index{ -1 };
            std::cin >> index;
            printLine( monasteries, count, index );
            break;
        }
        case ShowAll:
            printTable( monasteries, SIZE, count );
            break;
        default:
            std::cout << "Bad input. Try again." << std::endl;
            break;
        }
    }

    return 0;
}
